package com.zhifantech.aop;

import java.lang.reflect.Field;
import java.util.Collection;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.zhifantech.domain.annotation.EncryptField;
import com.zhifantech.domain.annotation.EncryptMethod;
import com.zhifantech.util.EncryptUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author ssliu
 * @date 2021-01-28
 */
@Slf4j
@Aspect
@Component
public class EncryptAspect {


  // @Pointcut("@annotation(com.zhifan.address_tag.domain.annotation.AddressParam)")
  // public void addressPointcut() {
  // }

  @Pointcut("execution(public * *(..))")
  public void publicMethod() {}


  @Pointcut("within(@com.zhifantech.domain.annotation.AddressParam *)")
  public void beanAnnotatedWithAddressParam() {}



  @Pointcut("publicMethod() && beanAnnotatedWithAddressParam()")
  public void publicMethodInsideAClassMarkedWithAddressParam() {}

  @Pointcut(" @annotation(com.zhifantech.domain.annotation.EncryptMethod)")
  public void methodMarkedWithEncryptMethod() {
  }

  @Around("methodMarkedWithEncryptMethod() && @annotation(encryptMethod)")
  public Object around(ProceedingJoinPoint joinPoint, EncryptMethod encryptMethod)
      throws Throwable {
    Object[] args = joinPoint.getArgs();

    if (encryptMethod.isEncrypt()) {
      for (Object arg : args) {
        if (arg instanceof Collection) {
          for (Object obj : (Collection) arg) {
            handleEncryptOrDecrypt(obj,true);
          }
        } else {
          handleEncryptOrDecrypt(arg,true);
        }
      }
    }
    Object result = joinPoint.proceed();
    // 需要解密
    if (!encryptMethod.isEncrypt()) {
      if (result instanceof Collection) {
        for (Object o : (Collection) result) {
          handleEncryptOrDecrypt(o,false);
        }
      } else if (result instanceof Page) {
        for (Object o : ((Page) result).getContent()) {
          handleEncryptOrDecrypt(o,false);
        }
      }else {
        handleEncryptOrDecrypt(result,false);
      }
    }
    return result;
  }

  /**
   * 将需要加密的参数进行加密
   * @param object
   * @throws IllegalAccessException
   */
  private void handleEncryptOrDecrypt(Object object,boolean encrypt) throws IllegalAccessException {
    if (ObjectUtils.isEmpty(object)) {
      return;
    }
    Field[] declaredFields = object.getClass().getDeclaredFields();
    for (Field declaredField : declaredFields) {
      if (declaredField.isAnnotationPresent(EncryptField.class)) {
        if (!declaredField.isAccessible()) {
          declaredField.setAccessible(true);
        }
        EncryptField encryptAnnotation = declaredField.getAnnotation(EncryptField.class);
        String rawValue = (String) declaredField.get(object);
        String resultValue;
        if (encryptAnnotation.isIndex()) {
          if (encrypt) {
            resultValue = EncryptUtils.encryptAddress(rawValue);
          } else {
            resultValue = EncryptUtils.decryptAddress(rawValue);
          }
        } else {
          if (encrypt) {
            resultValue = EncryptUtils.encrypt(rawValue, declaredField.getName());
          } else {
            resultValue = EncryptUtils.decrypt(rawValue, declaredField.getName());
          }
        }
        declaredField.set(object,resultValue);
      }
    }
  }

  @Around("publicMethodInsideAClassMarkedWithAddressParam()")
  public Object around(ProceedingJoinPoint joinPoint)
      throws Throwable {
    Object[] args = joinPoint.getArgs();
    // 1.检查address参数
    Signature signature = joinPoint.getSignature();
    if (signature instanceof MethodSignature) {
      String[] parameterNames = ((MethodSignature) signature).getParameterNames();
      int addressIndex = ArrayUtils.indexOf(parameterNames, "address");
      if (addressIndex != -1) {
        args[addressIndex] = DigestUtils.md5Hex((String) args[addressIndex]);
      }
    }
    return joinPoint.proceed(args);
  }



}
