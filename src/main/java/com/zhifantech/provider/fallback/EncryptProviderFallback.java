package com.zhifantech.provider.fallback;

import org.springframework.stereotype.Component;

import com.zhifantech.common.BaseResult;
import com.zhifantech.provider.EncryptProvider;

/**
 * @author ssliu
 * @date 2021-01-27
 */
@Component
public class EncryptProviderFallback implements EncryptProvider {

  @Override
  public BaseResult<String> encrypt(String plainText, String context) {
    return null;
  }

  @Override
  public BaseResult<String> decrypt(String encryptText, String context) {
    return null;
  }
}
