package com.zhifantech.provider;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.zhifantech.common.BaseResult;
import com.zhifantech.provider.fallback.EncryptProviderFallback;

/**
 * @author ssliu
 * @date 2021-01-27
 */
@FeignClient(value = "${providers.encrypt}", fallback = EncryptProviderFallback.class)
public interface EncryptProvider {

  /**
   * 加密
   * @param plainText
   * @param context
   * @return
   */
  @PostMapping("/api/encrypt")
  BaseResult<String> encrypt(@RequestParam String plainText,@RequestParam String context);

  /**
   * 解密
   * @param encryptText
   * @param context
   * @return
   */
  @PostMapping("/api/decrypt")
  BaseResult<String> decrypt(@RequestParam String encryptText,@RequestParam String context);


}
