package com.zhifantech.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.zhifantech.domain.entity.AddressSeedInfoEntity;
import com.zhifantech.domain.entity.CoinTypeEntity;
import com.zhifantech.domain.entity.LabelInfoEntity;
import com.zhifantech.repository.AddressSeedInfoRepository;
import com.zhifantech.repository.CoinTypeRepository;
import com.zhifantech.repository.LabelInfoRepository;
import com.zhifantech.service.ImportService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author ssliu
 * @date 2021-02-01
 */
@Service
@Slf4j
public class ImportServiceImpl implements ImportService {

  private static Map<String, Integer> labelNameIdMap = new HashMap<>(3000);

  private final CoinTypeRepository coinTypeRepository;
  private final LabelInfoRepository labelInfoRepository;
  private final AddressSeedInfoRepository addressSeedInfoRepository;

  public ImportServiceImpl(CoinTypeRepository coinTypeRepository,
      LabelInfoRepository labelInfoRepository,
      AddressSeedInfoRepository addressSeedInfoRepository) {
    this.coinTypeRepository = coinTypeRepository;
    this.labelInfoRepository = labelInfoRepository;
    this.addressSeedInfoRepository = addressSeedInfoRepository;
  }

  @Override
  public List<CoinTypeEntity> getCoinTypes() {
    buildLabelNameIdMap();
    return coinTypeRepository.findAll(Sort.by("coinId"));
  }

  @Override
  public void refreshLabel() {
    buildLabelNameIdMap();
    log.info("refresh label finish");

  }

  @Override
  public String importSeedAddress(String coinType, MultipartFile mf) throws Exception {

    // if (ObjectUtils.isEmpty(sourList)) {
    //   Map<String, String> sourceMap = new HashMap<>();
    //   sourceMap.put("num", "1");
    //   // sourceMap.put("labelId", "0");
    //   // sourceMap.put("name", "后台导入");
    //   // sourceMap.put("url","");
    //   sourceMap.put("type", "0");
    //   sourceMap.put("time", String.valueOf(System.currentTimeMillis()));
    //   sourList.add(sourceMap);
    // }

    // 1.读取数据
    BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(mf.getInputStream()));
    String line;
    List<String> fileList = new ArrayList<>(10000);
    while ((line = bufferedReader.readLine()) != null) {
      fileList.add(line);
    }
    // 2.解析数据
    List<AddressSeedInfoEntity> addressSeedInfoEntities =
        batchParseLine(coinType, fileList);
    // 3.存储数据
    addressSeedInfoRepository.saveAll(addressSeedInfoEntities);

    return "导入成功！共" + addressSeedInfoEntities.size() + "条数据";
  }

  /**
   * 批量解析line
   *
   * @param lines
   * @return
   */
  private List<AddressSeedInfoEntity> batchParseLine(String coinType, List<String> lines)
      throws Exception {
    ArrayList<AddressSeedInfoEntity> result =
        new ArrayList<>(lines.size());
    for (String line : lines) {
      String[] split;
      if (line.contains("\t")) {
        split = line.split("\\t");
      } else {
        split = line.split("\\s{4,}");
      }
      if (split.length != 2) {
        throw new Exception("上传失败，格式不正确， line:" + line);
      }
      try {
        result.add(buildTag(coinType, split[0].trim(), split[1].trim()));
      } catch (Exception e) {
        throw new Exception("上传失败，实体不存在，line：" + line);
      }
    }
    return result;
  }

  /**
   * 构建入库数据
   *
   * @param coinType
   * @param hash
   * @param entityValue
   * @return
   */
  private AddressSeedInfoEntity buildTag(String coinType, String hash,
      String entityValue) throws Exception {
    hash = hash.trim();
    //eth币种不区分大小写,库中全部存小写数据
    if ("eth".equals(coinType.toLowerCase())) {
      hash = hash.toLowerCase();
    }
    long now = System.currentTimeMillis();
    AddressSeedInfoEntity data = new AddressSeedInfoEntity();
    data.setAddress(hash);
    data.setCoinType(coinType);
    data.setCreateTime(now);
    data.setExaminTime(now);
    data.setExaminStatus(2);
    data.setStatus(1);
    data.setWalletType(0);
    // data.setSourceList(sourList);

    if (labelNameIdMap.containsKey(entityValue)) {
      Integer labelId = labelNameIdMap.get(entityValue.trim());
      data.setIsLable(1);
      data.setIsTag(Lists.newArrayList(labelId));
    } else {
      throw new Exception();
    }

    return data;
  }

  private void buildLabelNameIdMap() {
    List<LabelInfoEntity> labelList = labelInfoRepository.findAll();
    labelList.stream().forEach(item -> {
      labelNameIdMap.put(item.getLabelName(), item.getLabelId());
    });
  }
}
