package com.zhifantech.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.zhifantech.domain.entity.CoinTypeEntity;

/**
 * @author ssliu
 * @date 2021-02-01
 */
public interface ImportService {

  /**
   * 获取币种列表
   * @return
   */
  List<CoinTypeEntity> getCoinTypes();

  /**
   * 导入数据
   * @param coinType
   * @param mf
   * @return
   */
  String importSeedAddress(String coinType, MultipartFile mf) throws Exception;

  void refreshLabel();

}
