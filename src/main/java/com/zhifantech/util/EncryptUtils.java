package com.zhifantech.util;

import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.zhifantech.common.BaseResult;
import com.zhifantech.domain.entity.AddressDictionaryEntity;
import com.zhifantech.provider.EncryptProvider;
import com.zhifantech.repository.AddressDictionaryRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 加解密工具
 * @author ssliu
 * @date 2021-01-27
 */
@Component
@Slf4j
public class EncryptUtils {

  @Resource
  private EncryptProvider encryptProvider;

  @Resource
  private AddressDictionaryRepository addressDictionaryRepository;

  private static EncryptUtils instance = new EncryptUtils();

  /**
   * 默认上下文
   */
  private static final  String DEFAULT_CONTEXT = "chaindigg";

  @PostConstruct
  public void init() {
    instance.encryptProvider = encryptProvider;
    instance.addressDictionaryRepository = addressDictionaryRepository;
  }

  /**
   * 加密数据
   * @param plainText
   * @param context
   * @return
   */
  public static String encrypt(String plainText, String context) {
    if (ObjectUtils.isEmpty(plainText)) {
      return plainText;
    }
    BaseResult<String> result = instance.encryptProvider.encrypt(plainText, context);
    if ("0000".equals(result.getCode())) {
      return result.getData();
    }
    log.error("Error to encrypt,msg :{}",result.getDesc());
    return "";
  }

  /**
   * 解密数据
   * @param encryptText
   * @param context
   * @return
   */
  public static String decrypt(String encryptText, String context) {
    if (ObjectUtils.isEmpty(encryptText)) {
      return encryptText;
    }
    BaseResult<String> result = instance.encryptProvider.decrypt(encryptText, context);
    if ("0000".equals(result.getCode())) {
      return result.getData();
    }
    log.error("Error to decrypt,msg :{}",result.getDesc());
    return "";
  }

  public static String md5Address(String address) {
    return DigestUtils.md5Hex(address);
  }

  /**
   * 使用默认的上下文进行加密
   * @param plainText
   * @return
   */
  public static String encrypt(String plainText) {
    return encrypt(plainText,DEFAULT_CONTEXT);
  }

  /**
   * 使用默认的上下文进行解密
   * @param encryptText
   * @return
   */
  public static String decrypt(String encryptText) {
    return decrypt(encryptText,DEFAULT_CONTEXT);
  }

  /**
   * 地址解密
   * @param md5Address
   * @return
   */
  public static String decryptAddress(String md5Address) {
    Optional<AddressDictionaryEntity> byId =
        instance.addressDictionaryRepository.findById(md5Address);
    if (byId.isPresent()) {
      String encryptAddress = byId.get().getEncrypt();
      return  decrypt(encryptAddress, "address");
    }
    log.error("Error to decrypt address,msg : address md5 not found in db");
    return "";
  }

  /**
   * 地址数据加密，因为搜索的原因，所以地址数据统一使用md5
   * @param plainAddress
   * @return
   */
  public static String encryptAddress(String plainAddress) {
    String md5Address = DigestUtils.md5Hex(plainAddress);
    String encryptAddress = encrypt(plainAddress, "address");
    AddressDictionaryEntity addressDictionaryEntity =
        new AddressDictionaryEntity(md5Address, encryptAddress);
    instance.addressDictionaryRepository.save(addressDictionaryEntity);
    return md5Address;
  }

}
