package com.zhifantech.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zhifantech.domain.entity.CoinTypeEntity;
import com.zhifantech.service.ImportService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author ssliu
 * @date 2021-02-01
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class ImportController {

  @Autowired
  ImportService importService;

  @GetMapping("/coinTypes")
  public List<CoinTypeEntity> getCoinTypes() {
    return importService.getCoinTypes();
  }

  @PostMapping("/import")
  public String importData(@RequestParam String coinType, @RequestParam MultipartFile file) {
    try {
      return importService.importSeedAddress(coinType.toLowerCase(),file);
    } catch (Exception e) {
      log.error("",e);
      return e.getMessage();
    }
  }
}
