package com.zhifantech.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.zhifantech.service.ImportService;

/**
 * @author ssliu
 * @date 2021-02-02
 */
@Component
public class ScheduleTask {

  private final ImportService importService;

  public ScheduleTask(ImportService importService) {
    this.importService = importService;
  }

  @Scheduled(fixedDelay = 5000)
  public void refreshLabel() {
    importService.refreshLabel();
  }

}
