package com.zhifantech.repository;

import java.util.List;

import com.zhifantech.domain.annotation.EncryptMethod;
import com.zhifantech.domain.entity.LabelInfoEntity;

public interface LabelInfoRepository extends CommonEncryptRepository<LabelInfoEntity,Integer> {
    @EncryptMethod
    List<LabelInfoEntity> findByLabelNameLike(String msg);
    @EncryptMethod
    List<LabelInfoEntity> findByLabelIdIn(List<Integer> labelId);
    @EncryptMethod
    LabelInfoEntity findByLabelId(Integer labelId);
    @EncryptMethod
    LabelInfoEntity findByEntityIdAndLabelEntity(Integer entityId,Integer le);
    @EncryptMethod
    List<LabelInfoEntity> findByEntityIdIn(List<Integer> entityIdList);


}
