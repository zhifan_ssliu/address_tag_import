package com.zhifantech.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zhifantech.domain.entity.CoinTypeEntity;

public interface CoinTypeRepository  extends JpaRepository<CoinTypeEntity,Integer> {

  CoinTypeEntity findByCoinId(Integer coinId);


}
