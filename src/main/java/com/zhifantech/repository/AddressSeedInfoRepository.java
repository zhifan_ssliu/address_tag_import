package com.zhifantech.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.zhifantech.domain.annotation.EncryptMethod;
import com.zhifantech.domain.entity.AddressSeedInfoEntity;

@Repository
public interface AddressSeedInfoRepository extends
    CommonEncryptRepository<AddressSeedInfoEntity,String> {

    @EncryptMethod
    AddressSeedInfoEntity findByAddress(String address);
    @EncryptMethod
    Page<AddressSeedInfoEntity> findByAddress(String address,Pageable pageRequest);
    @EncryptMethod
    Page<AddressSeedInfoEntity> findByAddressAndCoinType(String address, String coinType, Pageable pageRequest);
    @EncryptMethod
    Page<AddressSeedInfoEntity> findByAddressAndIsLable(String address, Integer isLable, Pageable pageRequest);
    @EncryptMethod
    Page<AddressSeedInfoEntity> findByAddressAndIsLableAndCoinType(String address, Integer isLable,String coinType, Pageable pageRequest);
    @EncryptMethod
    Page<AddressSeedInfoEntity> findByCoinType(String coinType, Pageable pageRequest);
    @EncryptMethod
    Page<AddressSeedInfoEntity> findByIsLableAndCoinType(Integer isLabel,String coinType,Pageable pageRequest);

    @EncryptMethod
    Page<AddressSeedInfoEntity> findByIsLable(Integer isLabel,Pageable pageRequest);

    @EncryptMethod
    List<AddressSeedInfoEntity> findByExaminStatusAndStatus(Integer examinStatus,Integer status,Pageable pageRequest);
    @EncryptMethod
    List<AddressSeedInfoEntity> findByWalletType(Integer walletType);


}
