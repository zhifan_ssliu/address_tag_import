package com.zhifantech.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zhifantech.domain.entity.AddressDictionaryEntity;

/**
 * @author ssliu
 * @date 2021-01-27
 */
@Repository
public interface AddressDictionaryRepository extends JpaRepository<AddressDictionaryEntity,String> {
}
