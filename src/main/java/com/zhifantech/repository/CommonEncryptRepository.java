package com.zhifantech.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import com.zhifantech.domain.annotation.EncryptMethod;

/**
 * 通用加密Repository，用于在一些常用的方法上添加注解
 * @author ssliu
 * @date 2021-01-28
 */
@NoRepositoryBean
public interface CommonEncryptRepository<T, ID> extends JpaRepository<T, ID> {

  @Override
  @EncryptMethod
  Page<T> findAll(Pageable pageable);

  @Override
  @EncryptMethod
  List<T> findAll();

  @EncryptMethod(isEncrypt = true)
  @Override
  <S extends T> List<S> saveAll(Iterable<S> var1);

  @EncryptMethod(isEncrypt = true)
  @Override
  <S extends T> S save(S var1);

  @Override
  @EncryptMethod
  Optional<T> findById(ID id);
}
