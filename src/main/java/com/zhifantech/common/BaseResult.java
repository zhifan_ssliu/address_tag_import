package com.zhifantech.common;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一返回结果
 *
 * @author ssliu
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResult<T> implements Serializable {

  private String code;
  private String desc;
  private T data;

  public static <T> BaseResult<T> success(T data){
    return new BaseResult<>(ResultCode.SUCCESS.getCode(),ResultCode.SUCCESS.getMsg(),data);
  }

  public static  BaseResult<String> fail(String msg){
    return new BaseResult<>(ResultCode.SYSTEM_BUSY.getCode(),ResultCode.SYSTEM_BUSY.getMsg(),msg);
  }
}
