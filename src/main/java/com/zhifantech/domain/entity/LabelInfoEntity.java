package com.zhifantech.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zhifantech.domain.annotation.EncryptField;

import lombok.Data;

@Data
// @Document(indexName = "al_label_info", type = "al_label_info_type",shards = 3,replicas = 2)
@Table(name = "al_label_info")
@Entity
public class LabelInfoEntity {

    public static Integer LE_ONE = 1; //机构

    public static Integer LE_TWO = 2; //个人

    @Id
    private Integer labelId;

    private Integer entityId;  //机构Id

    @EncryptField
    private String labelName;

    private Integer dimensionId;

    private Integer labelEntity;

    private Long createTime;

    private Long modifyTime;

}
