package com.zhifantech.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "al_coin_type_info")
@Entity
public class CoinTypeEntity {



  @Id
  private Integer coinId;

  private String coinType;

  private Long createTime;

  private Long modifyTime;

}
