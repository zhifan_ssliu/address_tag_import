package com.zhifantech.domain.entity;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.util.ObjectUtils;

import com.google.gson.Gson;
import com.zhifantech.domain.annotation.EncryptField;
import com.zhifantech.util.DataUtil;

import lombok.Data;

@Data
// @Document(indexName = "al_address_seed_info", type = "al_address_seed_info_type",shards = 3,replicas = 2)
@Table(name = "al_address_seed_info")
@Entity
public class AddressSeedInfoEntity {

    private static Gson gson = new Gson();

    @Id
    // @Field(fielddata = true)
    @EncryptField(isIndex = true)
    private String address;

    @Transient
    private List<Integer> isTag;
    @Column(length = 1000)
    private String isTagDb;

    @Transient
    private List<Integer> isNotTag;
    @Column(length = 1000)
    private String isNotTagDb;

    private String coinType;

    @Transient
    private List<Map<String,String>> sourceList;
    @Column(length = 5000)
    @EncryptField
    private String sourceListDb;

    private Integer walletType;

    private Integer isLable;

    private Integer examinStatus;

    private Integer examinUserId;

    private Long examinTime;

    private String examinNote;

    private Integer status;

    private Long createTime;

    public List<Integer> getIsTag() {
        if (ObjectUtils.isEmpty(this.isTag)) {
            this.isTag = gson.fromJson(isTagDb, DataUtil.TYPE_LIST_INTEGER);
        }
        return this.isTag;
    }

    public void setIsTag(List<Integer> isTag) {
        this.isTag = isTag;
        this.isTagDb = gson.toJson(isTag);
    }

    public List<Integer> getIsNotTag() {
        if (ObjectUtils.isEmpty(this.isNotTag)) {
            this.isNotTag = gson.fromJson(isNotTagDb,DataUtil.TYPE_LIST_INTEGER);
        }
        return isNotTag;
    }

    public void setIsNotTag(List<Integer> isNotTag) {
        this.isNotTag = isNotTag;
        this.isNotTagDb = gson.toJson(isNotTag);
    }

    public List<Map<String, String>> getSourceList() {
        if (ObjectUtils.isEmpty(this.sourceList)) {
            this.sourceList = gson.fromJson(this.sourceListDb, List.class);
        }
        return sourceList;
    }

    public void setSourceList(List<Map<String, String>> sourceList) {
        this.sourceList = sourceList;
        this.sourceListDb  = gson.toJson(sourceList);
    }


}
