package com.zhifantech.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ssliu
 * @date 2021-01-27
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "al_address_dictionary")
public class AddressDictionaryEntity {
  @Id
  String address;

  @Column(length = 2000)
  String encrypt;


}
